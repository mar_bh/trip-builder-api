-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2018 at 04:03 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbtripbuilder`
--
CREATE DATABASE IF NOT EXISTS `dbtripbuilder` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dbtripbuilder`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_flights`
--

CREATE TABLE `tbl_flights` (
  `id` int(11) NOT NULL,
  `airline_name` varchar(500) NOT NULL,
  `price` float(8,2) NOT NULL,
  `flight_no` varchar(300) NOT NULL,
  `origin` varchar(250) NOT NULL,
  `destination` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_flights`
--

INSERT INTO `tbl_flights` (`id`, `airline_name`, `price`, `flight_no`, `origin`, `destination`) VALUES
(1, 'Southern Twayblade', 1512.25, 'THSO-25', 'Cuba', 'Thailand'),
(4, 'Santa Rita Mountain Draba', 1665.80, 'fEm-11', 'Canada', 'Brazil');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_flights`
--
ALTER TABLE `tbl_flights`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_flights`
--
ALTER TABLE `tbl_flights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
