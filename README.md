# Web service with codeigniter and phpunit setup. 
## Simulate flights operations



### Functionalities


1.  Alphabetical listing of real airports in the world. (Used https://aviation-edge.com/ API to get data)


2.  Get available flights for one trip. (Simulated with random data) 


3.  List of flights added to a trip.


4.  Add flights to a trip.


5.  Delete flights to a trip.




### Requirements


PHP PHP 7.1


PHPUnit 7.0.1



### Installation.



Clone repository


Open config.php file in appx/config folder and go to line 26 and change base_url  to your url
$config['base_url'] = 'http://localhost';



Open database.php file in appx/config folder and replace with your database connectivity settings.



Database 


/database/dbtripbuilder.sql



Run UnitTests


Install PHPUnit 


$ cd /path/to/project/


$ cd appx/tests/


$ phpunit






### To test API (Postman recommended)


Select HTTP method.



GET


http://localhost/trip-builder-api/api/airport/list (Data from https://aviation-edge.com/)


http://localhost/trip-builder-api/api/search/flights


Params


origin,
destination


http://localhost/trip-builder-api/api/flight/listFlights


http://localhost/trip-builder-api/api/flight/flightByNo


Param


flight_no



POST


http://localhost/trip-builder-api/api/flight/addFlight


Params


airline_name,    
price     ,
flight_no   ,
origin  ,
destination ,

DELETE


http://localhost/trip-builder-api/api/flight/deleteFlight


Param


flight_no




