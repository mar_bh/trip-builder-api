<?php
  class Flight_model extends CI_Model {
       
      public function __construct(){
          
        $this->load->database();
        
      }
      
      //API call - get a flight record by number
      public function getFlightByNo($flight_no){  

           $this->db->select('id, airline_name, price, flight_no, origin, destination');
           $this->db->from('tbl_flights');
           $this->db->where('flight_no', $flight_no);
           $query = $this->db->get();
           
           if($query->num_rows() == 1){
               return $query->result_array();
           }
           else{
             return 0;
          }
      }

    //API call - get flights record
    public function getAllFlights(){   
        $this->db->select('id, airline_name, price, flight_no, origin, destination');
        $this->db->from('tbl_flights');
        $this->db->order_by("id"); 
        $query = $this->db->get();

        if($query->num_rows() > 0){
          return $query->result_array();
        }else{
          return 0;
        }
    }
   
   //API call - delete a flight record
    public function delete($flight_no){
       $this->db->where('flight_no', $flight_no);
       if($this->db->delete('tbl_flights')){
          return true;
        }else{
          return false;
        }
   }
   
   //API call - add new flight record
    public function add($data){
        if($this->db->insert('tbl_flights', $data)){
           return true;
        }else{
           return false;
        }
    }
    
    //API call - update a flight record
    public function update($id, $data){
       $this->db->where('id', $id);
       if($this->db->update('tbl_flights', $data)){
          return true;
        }else{
          return false;
        }
    }
}