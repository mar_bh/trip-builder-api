<?php
class Flight_model_test extends TestCase
{
	public function setUp()
	{
		$this->obj = $this->newModel('Flight_model');
	}

	public function test_getflightbyno(){
		$actual = $this->obj->getflightbyno('THSO-25');
		// $expected = '[{"id":"1","airline_name":"Southern Twayblade","price":"1512.25","flight_no":"THSO-25","origin":"Cuba","destination":"Thailand"}]';
        $expected = array(
			"id"=>"1",
			"airline_name"=>"Southern Twayblade",
			"price"=>"1512.25",
			"flight_no"=>"THSO-25",
			"origin"=>"Cuba",
			"destination"=>"Thailand"
		);
        $this->assertEquals($expected, $actual[0]);
	}
}