<?php


class Flight_test extends TestCase{

	public function test_flight_get(){
		try {
			$output = $this->request(
				'GET', 'api/flight/listFlights'
			);
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
		}
		$expected = '[{"id":"1","airline_name":"Southern Twayblade","price":"1512.25","flight_no":"THSO-25","origin":"Cuba","destination":"Thailand"},{"id":"4","airline_name":"Santa Rita Mountain Draba","price":"1665.80","flight_no":"fEm-11","origin":"Canada","destination":"Brazil"}]';
		$this->assertEquals($expected, $output);
		$this->assertResponseCode(200);
	}

	public function test_flight_post(){
		try {
			$output = $this->request(
				'POST', 'api/flight/addFlight?airline_name=Santa Rita Mountain Draba&price=1665.80&flight_no=fEm-11&origin=Canada&destination=Brazil',
				[
					'airline_name' => 'test insert',
					'price' => '1000',
					'flight_no' => 'xyz',
					'origin' => 'xyz',
					'destination' => 'abc',
				]
			);
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
		}
		
		$expected = '"success"';
		$this->assertEquals($expected, $output);
		$this->assertResponseCode(200);		
	}

	public function test_flight_delete(){
		
		try {
			$output = $this->request(
				'DELETE', 'api/flight/deleteFlight?flight_no=xyz','flight_no=xyz'
			);
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
		}
		
		$expected = '"success"';
		$this->assertEquals($expected, $output);
		$this->assertResponseCode(200);		
	}

	public function test_error_flight_post(){
		try {
			$output = $this->request(
				'POST', 'api/flight/addFlight'
				
			);
		} catch (CIPHPUnitTestExitException $e) {
			$output = ob_get_clean();
		}
		
		$expected = '"Please enter complete flight information to add it to your trip"';
		$this->assertEquals($expected, $output);
		$this->assertResponseCode(400);		
	}

}