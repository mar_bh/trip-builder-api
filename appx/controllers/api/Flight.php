<?php

require_once(APPPATH.'/libraries/REST_Controller.php');
 
class Flight extends REST_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('flight_model');
    }

    //API - customer sends number and on valid flight number information is sent back
    function flightByNo_get(){

        $flight_no  = $this->get('flight_no');
        
        if(!$flight_no){
            $this->response("No flight number specified", 400);            
        }

        $result = $this->flight_model->getFlightByNo( $flight_no );
        if($result){
            $this->response($result, 200);            
        }else{
             $this->response("Invalid flight number", 404);           
        }
    } 

    //API -  Fetch All flights
    function listFlights_get(){

        $result = $this->flight_model->getAllFlights();
        if($result){
            $this->response($result, 200); 
        } 
        else{
            $this->response("No record found", 404);
        }
    }
     
    //API - add a new flight item in database for the trip.
    function addFlight_post(){

        $airline_name      = $this->post('airline_name');
        $price     = $this->post('price');
        $flight_no    = $this->post('flight_no');
        $origin  = $this->post('origin');
        $destination  = $this->post('destination');         
    
        if(!$airline_name || !$price || !$flight_no || !$origin || !$destination ){
            $this->response("Please enter complete flight information to add it to your trip", 400);
        }else{
            $result = $this->flight_model->add(array("airline_name"=>$airline_name, "price"=>$price, "flight_no"=>$flight_no, "origin"=>$origin, "destination"=>$destination));
        if($result === 0){
            $this->response("Flight information coild not be saved. Try again.", 404);
        }else{
            $this->response("success", 200);             
            }
        }
    }

    //API - update a flight 
    function updateFlight_put(){  

         $airline_name = $this->put('airline_name');
         $price = $this->put('price');
         $flight_no = $this->put('flight_no');
         $origin = $this->put('origin');
         $destination = $this->put('destination');
         $id = $this->put('id');
         
         if(!$airline_name || !$price || !$flight_no || !$origin || !$destination ){
                $this->response("Enter complete flight information to save", 400);
         }else{
            $result = $this->flight_model->update($id, array("airline_name"=>$airline_name, "price"=>$price, "flight_no"=>$flight_no, "origin"=>$origin, "destination"=>$destination));
            if($result === 0){
                $this->response("Flight information coild not be saved. Try again.", 404);
            }else{
                $this->response("success", 200);  
            }
        }
    }

    //API - delete a flight 
    function deleteFlight_delete(){

        $flight_no  = $this->delete('flight_no');

        if(!$flight_no){
            $this->response("Parameter missing", 404);
        }
         
        if($this->flight_model->delete($flight_no)){
            $this->response("success", 200);
        } 
        else{
            $this->response("Failed", 400);
        }
    }
}