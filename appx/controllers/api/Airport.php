<?php

require(APPPATH.'/libraries/REST_Controller.php');
 
class Airport extends REST_Controller{
    
    public function __construct(){
        parent::__construct();
    }
  
    //https://aviation-edge.com API to get airport data
    function list_get(){
       
        $service_url = 'https://aviation-edge.com/api/public/airportDatabase?key=17b099-a7b67b-8cd1af-76b89f-9f8810';
        
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            $this->response('error occured during curl exec', 404);
        }
        curl_close($curl);
        $decoded  = json_decode($curl_response);
        if (isset($decoded ->response->status) && $decoded ->response->status == 'ERROR') {
            die('error occured: ' . $decoded ->response->errormessage);
        }

        $result = array_column($decoded , 'nameAirport');
        sort($result);
        $this->response($result, 200);  
             
    }

    

}