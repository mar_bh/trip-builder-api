<?php

require(APPPATH.'/libraries/REST_Controller.php');
 
class Search extends REST_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
    }


    //API -  Fetch All possibles flights, 
    // Return random list simulating possibles flights (airline, flight-number) for customers to select and add to their trip.
    function flights_get(){

        $origin  = $this->get('origin');
        $destination  = $this->get('destination');
        
        if(!$origin || !$destination){
            $this->response("You must specify origin and destination", 400);
            exit;
        }

        $site_url = site_url() ;
        $string = file_get_contents($site_url . "assets/data/flights/MOCK_DATA.json");
        $all_flights = json_decode($string, true);

        $num_flights_found= rand(0, 50);

        if($num_flights_found==0){
                $this->response("No flights found", 400);
        }else{
            $rand_keys = array_rand($all_flights,$num_flights_found);
            
            for ($i = 0; $i < count($rand_keys); $i++) {
                    $result[] = $all_flights[$rand_keys[$i]];
                }
            $this->response($result, 200);
        }
    }
}